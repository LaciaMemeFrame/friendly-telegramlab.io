---
title: Let Me Google That For You
layout: default
parent: Modules
---

# {{ page.title }}

## Commands

- **Educate people about search engines.**
[Syntax: `lmgtfy <search term>`]
  
  Generates a short link to the LMGTFY meme of the search term specified, handy when rarts spam you for obvious answers.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4ODA0ODYxMzVdfQ==
-->
